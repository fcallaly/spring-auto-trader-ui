FROM nginxinc/nginx-unprivileged:latest

EXPOSE 8080

COPY dist/trader-ui/* /usr/share/nginx/html/
